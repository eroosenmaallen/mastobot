## MastoBot

NodeJS Mastodon client library with an eye to making bot development fun & easy.

This is an early-stage project, but there is [documentation](https://eroosenmaallen.gitlab.io/mastobot/)!

You can follow development, submit issues, and contribute at [GitLab](https://gitlab.com/eroosenmaallen/mastobot)

You can support the developer at [Ko-Fi](https://ko-fi.com/silvermoon82)


### Installation 

```npm i mastobot```


### Basic Usage

```javascript

const { MastoBot } = require('mastobot');

const api_url    = 'https://botsin.space/';
const api_token  = 'your API Token goes here';

const mb = new MastoBot(api_url, {
  api_token,
});

// The MastoBotHttp class offers helpers for HTTP requests.
// Wrappers for relevant HTTP methods are complete and fully usable:
// get, post, put, patch, delete
// They all expect the same parameters:
//   url      {string}    Path to the API endpoint, including the v1/ or v2/ prefix
//   data     {Object}    Properties will first populate :params in the path, remaining
//                        properties will be sent in the query string (GET) or request body
//   options  {Object}    Additional options, like headers. Rarely used.
mb.get('v1/accounts/verify_credentials')
.then(response => {
  console.log('Credentials are good!', response.data.acct);
})
.catch(apiError => {
  console.error('Credentials are bad:',
    apiError.statusCode,
    apiError.statusText,
    apiError.data.error);
});

mb.patch('v1/accounts/update_credentials', {
  'source[language]': 'en',
  'bot': true,
}).then(response => {
  console.log('Updated credentials successfully!', response.data);
}).catch(apiError => {
  console.error('Patch failed:',
    apiError.statusCode,
    apiError.statusText,
    apiError.data);
});


// The MastoBotAPI class builds on MastoBotHttp and offers fancier wrappers
// for API endpoints. At present, the most commonly used endpoints are
// covered. More will come.
mb.postStatus('Wow, MastoBot is really fun and easy!', {
  visibility: 'public',
})
.then(response => {
  console.log('Posted status successfully!', response.data.url);
})
.catch(apiError => {
  console.error('Post failed:',
    apiError.statusCode,
    apiError.statusText,
    apiError.data);
});


// NYI: The full MastoBot class presents all of the above, as well as
// a "smart" callback API for certain API calls:
mb.processNotifications({
  onMention: notification => mentionCallback(notification),
  onFollow: notification => followCallback(notification),
  onFavourite: notification => favouriteCallback(notification),
})
.then(response => {
  // response is an array of callback return values/Promises
  console.log('Processed notifications successfully!', response.length);
})
.catch(apiError => {
  console.error('Processing notification(s) failed:', apiError):
});
```

