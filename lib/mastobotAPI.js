/**
 *  MastoBot API Wrapper class
 *
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       May 2022
 */

'use strict';

const { MastoBotHttp } = require('./mastobotHttp');



/**
 *  MastoBotAPI -- API Endpoint wrappers
 */
class MastoBotAPI extends MastoBotHttp {
  /**
   *  @param {string}   api_url   URL of the instance to connect to; should
   *    include "https:" but not "api/v1/"
   *
   *  @param {Object}   options
   *  @param {string=}  options.api_token   User access token
   *  @param {string=}  options.userAgent   to override the user-agent provided
   *  @param {string=}  options.client_id     oAuth client ID
   *  @param {string=}  options.client_secret oAuth client secret
   */
  constructor(api_url = 'https://botsin.space/', options = {}) {
    super(api_url, options);

    Object.assign(this._api, {
      client_id: options.client_id,
      client_secret: options.client_secret,
    });
  }


  // ====================
  // API Helpers
  
  // https://docs.joinmastodon.org/methods/apps/
  
  /**
   *  @typedef {Object} CreateAppResponse
   *  @property {string}  client_id
   *  @property {string}  client_secret
   *  @inner
   */

  /**
   *  Create an Application in the target instance
   *
   *  @param  {string} client_name Name for the new client app
   *  @param  {Object} options     Additional/optional parameters:
   *  @param  {string} options.redirect_uris    Optional URI to redirect to
   *  @param  {string} options.scopes           oAuth scopes
   *  @param  {string} options.website          Application's Website
   *  @return {APIResponse<CreateAppResponse>} Server will return an oAuth
   *    client_id and client_secret; save these for future use
   *    @property data {CreateAppResponse}
   */
  async createApp(client_name, options = {}) {
    return this.post('api/v1/apps', {
      client_name,
      redirect_uris: options.redirect_uris || 'urn:ietf:wg:oauth:2.0:oob',
      scopes: options.scopes || undefined,
      website: options.website || undefined,
    }, {
      public: true,
    });
  }
  
  /**
   *  Verify your app works
   *
   *  @return {APIResponse<Application>} Response data will contain app name, website, and 
   *                                 vapid_key
   */
  async verifyApp() {
    return this.get('api/v1/apps/verify_credentials');
  }


  // https://docs.joinmastodon.org/methods/apps/oauth/
  
  
  /**
   *  @typedef {Object}  AuthorizeUserResponse
   *  @property {string} code         Authorization code
   *  @inner
   */
  

  /**
   *  Display an authorization form to the user, possibly requesting they log 
   *  in, then return an authorization code which can be used to request a user 
   *  token
   *
   *  @param  {string} client_id oAuth client ID for the app
   *  @param  {Object} options   Optional paramters for the request
   *  @return {APIResponse<AuthorizeUserResponse>}
   */
  async authorizeUser(client_id = this._api.client_id, options = {}) {
    return this.post('oauth/authorize', {
      response_type: 'code',
      client_id,
      redirect_uri: options.redirect_uri || 'urn:ietf:wg:oauth:2.0:oob',
      scope: options.scope || undefined,
      force_login: options.force_login || undefined,
    }, {
      public: true,
    });
  }

  /**
   *  Obtain a user access token for user-scoped not-public stuff
   *
   *  @param  {string} code          User's authorization code (@see authorizeUser)
   *  @param  {string} client_id     oAuth client_id for the app
   *  @param  {string} client_secret oAuth client_secret for the app
   *  @param  {Object} options       Extended/optional paramters
   *  @return {APIResponse}          Will contain `access_token`
   */
  async obtainToken(
    code,
    client_id = this._api.client_id,
    client_secret = this._api.client_secret,
    options = {}
  ) {
    return this.post('oauth/token', {
      grant_type: 'authorization_code',
      client_id,
      client_secret,
      redirect_uri: options.redirect_uri || 'urn:ietf:wg:oauth:2.0:oob',
      scope: options.scope || undefined,
      code,
    }, {
      public: true,
    });
  }

  /**
   *  Revoke an oAuth token
   *
   *  @param  {string} token         Access token
   *  @param  {string} client_id     oAuth client_id
   *  @param  {string} client_secret oAuth client secret
   *  @return {APIResponse}          response.data will be empty on success
   */
  async revokeToken(
    token,
    client_id = this._api.client_id,
    client_secret = this._api.client_secret
  ) {
    return this.post('oauth/revoke', {
      client_id,
      client_secret,
      token,
    });
  }

  
  // https://docs.joinmastodon.org/methods/accounts/#account-credentials

  /**
   *  Register a new account on the instance
   *
   *  @param  {Object} user User
   *  @param  {string} user.username    Desired username
   *  @param  {string} user.email       Email address
   *  @param  {string} user.password    Password
   *  @param  {boolean} user.agreement  User agrees to local rules & policies?
   *  @param  {string} user.locale      Language of the confirmation email; default 'en'
   *  @param  {string} user.reason      If registration requires manual approval, this 
   *                                    text will be shown to the moderator
   *  @return {APIResponse<Token>  The response will carry an access token for the new account
   */
  async registerAccount(user = {}) {
    return this.post('api/v1/accounts', {
      username: user.username,
      email: user.email,
      password: user.password,
      agreement: user.agreement,
      locale: user.locale,
      reason: user.reason || undefined,
    });
  }

  /**
   *  Validate user's credentials; return their account
   *
   *  @return {APIResponse<AccountWithSource>} Response data will contain the user's Account
   */
  async checkCredentials() {
    return this.get('api/v1/accounts/verify_credentials');
  }

  /**
   *  Update user's profile
   *
   *  @param  {Object} data Fields to update
   *  @param  {boolean} data.discoverable   If the account should be shown in the profile directory
   *  @param  {boolean} data.bot            If the account is a bot
   *  @param  {string} data.display_name    Profile display name
   *  @param  {string} data.note            Account bio
   *  @param  {string} data.avatar          avatar image, encoded multipart/form-data (NYI?)
   *  @param  {string} data.header          Header image, encoded multipart/form-data (NYI?)
   *  @param  {boolean} data.locked         If follows require request-approval
   *  @param  {Object} data.source
   *  @param  {string} data.source.privacy  Default status visibility
   *  @param  {boolean} data.source.sensitive If media & posts are sensitive by default
   *  @param  {string} data.source.language Default iso 6391 language for posts
   *  @param  {string} data.fields_attributes Profile metadata `name` and `value`
   *  @return {APIResponse<AccountWithSource>}      Response data will contain the user's updated Account
   */
  async updateCredentials(data = {}) {
    return this.patch('api/v1/accounts/update_credentials', data);
  }


  // https://docs.joinmastodon.org/methods/accounts/#retrieve-information

  /**
   *  View a profile
   * 
   *  @param {string} id    Account id
   *  @return {APIResponse<Account>}
   */
  async getAccount(id) {
    return this.get(`v1/accounts/:id`, { id });
  }

  /**
   *  Get posts by an account
   * 
   *  @param {string} id    Account id
   *  @param {object} options
   *  @param {number=} options.limit  Limit number of results per fetch
   *  @return {APIResponse<Status[]>}
   */
  async getAccountStatuses(id, options = {}) {
    return this.get('api/v1/accounts/:id/statuses', {
      id,
      max_id: options.max_id || undefined,
      since_id: options.since_id || undefined,
      limit: options.limit || undefined,
    });
  }

  /**
   *  Get followers of an account
   * 
   *  @param {string} id    Account id
   *  @param {object} options
   *  @param {number=} options.limit  Limit number of results per fetch
   *  @return {APIResponse<Account[]>}
   */
  async getAccountFollowers(id, options = {}) {
   return this.get('api/v1/accounts/:id/followers', {
      id,
      max_id: options.max_id || undefined,
      since_id: options.since_id || undefined,
      limit: options.limit || undefined,
    });
  }

  /**
   *  Get accounts followed by an account
   * 
   *  @param {string} id    Account id
   *  @param {object} options
   *  @param {number=} options.limit  Limit number of results per fetch
   *  @return {APIResponse<Account[]>}
   */
  async getAccountFollowing(id, options = {}) {
   return this.get('api/v1/accounts/:id/following', {
      id,
      max_id: options.max_id || undefined,
      since_id: options.since_id || undefined,
      limit: options.limit || undefined,
    });
  }

  /** 
   *  Get featured tags for an account
   * 
   *  @param {string} id    Account id
   *
   *  @return {APIResponse<Tag[]>}
   */
  async getAccountFeaturedTags(id) {
   return this.get('api/v1/accounts/:id/featured_tags', { id });
  }

  /** 
   *  Get lists containing an account
   * 
   *  @param {string} id    Account id
   *
   *  @return {APIResponse<List[]>}
   */
  async getAccountLists(id) {
   return this.get('api/v1/accounts/:id/lists', { id });
  }

  /** 
   *  Get identity proofs for an account
   * 
   *  @param {string} id    Account id
   *
   *  @return {APIResponse<IdentityProof[]>}
   */
  async getAccountIdentityProofs(id) {
   return this.get('api/v1/accounts/:id/identity_proofs', { id }) ;
  }


  // https://docs.joinmastodon.org/methods/accounts/#retrieve-information

  async followAccount(id) {
    return this.post('api/v1/accounts/:id/follow', { id });
  }

  async unfollowAccount(id) {
    return this.post('api/v1/accounts/:id/unfollow', { id });
  }

  async blockAccount(id) {
    return this.post('api/v1/accounts/:id/block', { id });
  }

  async unblockAccount(id) {
    return this.post('api/v1/accounts/:id/unblock', { id });
  }

  async muteAccount(id) {
    return this.post('api/v1/accounts/:id/mute', { id });
  }

  async unmuteAccount(id) {
    return this.post('api/v1/accounts/:id/unmute', { id });
  }

  async pinAccount(id) {
    return this.post('api/v1/accounts/:id/pin', { id });
  }

  async unpinAccount(id) {
    return this.post('api/v1/accounts/:id/unpin', { id });
  }

  async setAccountNote(id, comment) {
    return this.post('api/v1/accounts/:id/note', { comment });
  }


  // https://docs.joinmastodon.org/methods/accounts/#general-account-actions
  
  async getAccountRelationships(id = []) {
    return this.get('api/v1/accounts/relationships', { id });
  }

  async searchAccounts(q, options = {}) {
    return this.get('api/v1/accounts/search', {
      q,
      limit: options.limit,
      resolve: options.resolve,
      following: options.following,
    });
  }


  // https://docs.joinmastodon.org/methods/accounts/bookmarks/
  
  /**
   *  Fetch user's bookmarked statuses
   *
   *  @param  {Object} options
   *  @param  {string} options.limit    Numer of results
   *  @param  {string} options.max_id   Highest id to return
   *  @param  {string} options.since_id Lowest id to return (?)
   *  @param  {string} options.min_id   Lowest id to return (?)
   *  @return {APIResponse<Status[]>}             Response data will be an array of bookmarked statuses
   */
  async getBookmarks(options = {}) {
    return this.get('api/v1/bookmarks', {
      limit: options.limit,
      max_id: options.max_id,
      since_id: options.since_id,
      min_id: options.min_id,
    });
  }


  // https://docs.joinmastodon.org/methods/accounts/favourites/
  
  /**
   *  Fetch user's favourited statuses
   *
   *  @param  {Object} options
   *  @param  {string} options.limit    Numer of results
   *  @param  {string} options.max_id   Highest id to return
   *  @param  {string} options.min_id   Lowest id to return
   *  @return {APIResponse<Status>}             Response data will be an array of favourited statuses
   */
  async getFavourites(options = {}) {
    return this.get('api/v1/favourites', {
      limit: options.limit,
      max_id: options.max_id,
      min_id: options.min_id,
    });
  }


  // https://docs.joinmastodon.org/methods/accounts/mutes/

  /**
   *  Fetch user's muted accounts
   *
   *  @param  {Object} options
   *  @param  {string} options.limit    Numer of results
   *  @param  {string} options.max_id   Highest id to return
   *  @param  {string} options.min_id   Lowest id to return
   *  @return {APIResponse<Account[]>}             Response data will be an array of muted Accounts
   */
  async getMutes(options = {}) {
    return this.get('api/v1/mutes', {
      limit: options.limit,
      max_id: options.max_id,
      min_id: options.min_id,
    });
  }


  // https://docs.joinmastodon.org/methods/accounts/blocks/

  /**
   *  Fetch user's blocked accounts
   *
   *  @param  {Object} options
   *  @param  {string} options.limit    Numer of results
   *  @param  {string} options.max_id   Highest id to return
   *  @param  {string} options.min_id   Lowest id to return
   *  @return {APIResponse<Account[]>}             Response data will be an array of blocked Accounts
   */
  async getBlocks(options = {}) {
    return this.get('api/v1/blocks', {
      limit: options.limit,
      max_id: options.max_id,
      min_id: options.min_id,
    });
  }


  // https://docs.joinmastodon.org/methods/accounts/domain_blocks/
  
  /**
   *  Fetch user's blocked domains
   *
   *  @param  {Object} options
   *  @param  {string} options.limit    Numer of results
   *  @param  {string} options.max_id   Highest id to return
   *  @param  {string} options.min_id   Lowest id to return
   *  @return {APIResponse<Account[]>}             Response data will be an array of blocked domains
   */
  async getDomainBlocks(options = {}) {
    return this.get('api/v1/domain_blocks', {
      limit: options.limit,
      max_id: options.max_id,
      min_id: options.min_id,
    });
  }

  /**
   *  Block a domain
   *
   *  @param  {string} domain Domain to block
   *  @return {APIResponse<{}>}        Response data will contain an empty object
   */
  async blockDomain(domain) {
    return this.post('api/v1/domain_blocks', {
      domain,
    });
  }

  /**
   *  Unblock a domain
   *
   *  @param  {string} domain Domain to unblock
   *  @return {APIResponse<{}>}        Response data will contain an empty object
   */
  async unblockDomain(domain) {
    return this.delete('api/v1/domain_blocks', {
      domain,
    });
  }


  // https://docs.joinmastodon.org/methods/accounts/filters/
  
  async getFilters() {
    return this.get('api/v1/filters');
  }

  async getFilter(id) {
    return this.get('api/v1/filters/:id', { id });
  }

  /**
   *  Create a new filter
   *
   *  @param  {string} phrase  Word or phrase to filter
   *  @param  {string[]} context One or more of home, notifications, public, thread. Default: all
   *  @param  {Object} options
   *  @param  {string} options.irreversible Should matching entities be dropped entirely?
   *  @param  {string} options.whole_word   Consider word boundaries?
   *  @param  {number} options.expires_in   Number of seconds before the filter automatically expires
   *  @return {APIResponse}                 Response data will contain the new Filter
   */
  async createFilter(phrase, context = ['home', 'notifications', 'public', 'thread'], options = {}) {
    return this.post('api/v1/filters', {
      phrase,
      context,
      irreversible: options.irreversible,
      whole_word: options.whole_word,
      expires_in: options.expires_in,
    })
  }

  /**
   *  Update a filter
   *
   *  @param  {string} id       ID of the filter to update
   *  @param  {string} phrase  Word or phrase to filter
   *  @param  {string[]} context One or more of home, notifications, public, thread. Default: all
   *  @param  {Object} options
   *  @param  {string} options.irreversible Should matching entities be dropped entirely?
   *  @param  {string} options.whole_word   Consider word boundaries?
   *  @param  {number} options.expires_in   Number of seconds before the filter automatically expires
   *  @return {APIResponse}                 Response data will contain the new Filter
   */
  async UpdateFilter(id, phrase, context = ['home', 'notifications', 'public', 'thread'], options = {}) {
    return this.put('api/v1/filters', {
      id,
      phrase,
      context,
      irreversible: options.irreversible,
      whole_word: options.whole_word,
      expires_in: options.expires_in,
    })
  }


  // @TODO: many more accounts/ endpoints
  

  // https://docs.joinmastodon.org/methods/statuses/
  
  async postStatus(status, options = {}) {
    return this.post('api/v1/statuses', {
      status,
      ...options,
    });
  }

  async getStatus(id) {
    return this.get('api/v1/statuses/:id', { id });
  }

  async deleteStatus(id) {
    return this.delete('api/v1/statuses/:id', { id });
  }

  async getStatusContext(id) {
    return this.get('api/v1/statuses/:id', { id });
  }

  async getStatusRebloggedBy(id) {
    return this.get('api/v1/statuses/:id/reblogged_by', { id });
  }

  async getStatusFavouritedBy(id) {
    return this.get('api/v1/statuses/:id/favourited_by', { id });
  }

  async favouriteStatus(id) {
    return this.post('api/v1/statuses/:id/favourite', { id });
  }

  async unfavouriteStatus(id) {
    return this.post('api/v1/statuses/:id/unfavourite', { id });
  }

  async reblogStatus(id) {
    return this.post('api/v1/statuses/:id/reblog', { id });
  }

  async unreblogStatus(id) {
    return this.post('api/v1/statuses/:id/unreblog', { id });
  }

  async bookmarkStatus(id) {
    return this.post('api/v1/statuses/:id/bookmark', { id });
  }

  async unbookmarkStatus(id) {
    return this.post('api/v1/statuses/:id/unbookmark', { id });
  }

  async muteStatus(id) {
    return this.post('api/v1/statuses/:id/mute', { id });
  }

  async unmuteStatus(id) {
    return this.post('api/v1/statuses/:id/unmute', { id });
  }

  async pinStatus(id) {
    return this.post('api/v1/statuses/:id/pin', { id });
  }

  async unpinStatus(id) {
    return this.post('api/v1/statuses/:id/unpin', { id });
  }

  // @todo: https://docs.joinmastodon.org/statuses/media/
  
  // @todo: https://docs.joinmastodon.org/statuses/polls/
  
  // @todo: https://docs.joinmastodon.org/statuses/scheduled_statuses/
  


  // https://docs.joinmastodon.org/methods/timelines/

  async getPublicTimeline(options = {}) {
    return this.get('api/v1/timelines/public', options);
  }

  async getHashtagTimeline(hashtag, options = {}) {
    return this.get('api/v1/timelines/tag/:hashtag', {
      hashtag,
      ...options,
    });
  }

  async getHomeTimeline(options = {}) {
    return this.get('api/v1/timelines/home', options);
  }

  async getListTimeline(list_id, options = {}) {
    return this.get('api/v1/timelines/list/:list_id', {
      list_id,
      ...options,
    });
  }


  // https://docs.joinmastodon.org/methods/timelines/conversations/

  async getConversations(options = {}) {
    return this.get('api/v1/conversations', options);
  }

  async deleteConversation(id) {
    return this.delete('api/v1/conversations/:id', { id });
  }

  async markConversationRead(id) {
    return this.post('api/v1/conversations/:id/read', { id });
  }


  // https://docs.joinmastodon.org/methods/timelines/lists/

  async getLists() {
    return this.get('api/v1/lists');
  }

  async getList(id) {
    return this.get('api/v1/lists/:id', { id });
  }

  async createList(title, { replies_policy = undefined }) {
    return this.post('api/v1/lists', {
      title,
      replies_policy,
    });
  }

  async updateList(id, title, { replies_policy = undefined }) {
    return this.put('api/v1/lists/:id', {
      id,
      title,
      replies_policy,
    });
  }

  async deleteList(id) {
    return this.delete('api/v1/lists/:id', { id });
  }

  async getAccountsInList(id, options = {}) {
    return this.get('api/v1/lists/:id/accounts', {
      id,
      max_id: options.max_id,
      since_id: options.since_id,
      limit: options.limit,
    });
  }

  async addAccountsToList(id, account_ids = []) {
    return this.post('api/v1/lists/:id/accounts', {
      id,
      account_ids,
    });
  }

  async deleteAccountsFromList(id, account_ids = []) {
    return this.delete('api/v1/lists/:id/accounts', {
      id,
      account_ids,
    });
  }


  // https://docs.joinmastodon.org/methods/timelines/markers/
  
  async getMarkers(timeline = ['home','notifications']) {
    return this.get('api/v1/markers', { timeline });
  }

  async saveMarkers(options = {}) {
    return this.post('api/v1/markers', {
      "home[last_read_id]": options.home,
      "notifications[last_read_id]": options.notifications,
    });
  }


  // @todo: https://docs.joinmastodon.org/methods/timelines/streaming/
  


  // https://docs.joinmastodon.org/methods/notifications/

  async getNotifications(options = {}) {
    return this.get('api/v1/notifications', options);
  }

  async getNotification(id) {
    return this.get('api/v1/notifications/:id', { id });
  }

  async dismissAllNotifications() {
    return this.post('api/v1/notifications/clear');
  }

  async dismissNotification(id) {
    return this.post('api/v1/notifications/:id/dismiss', { id });
  }


  // @todo: https://docs.joinmastodon.org/methods/notifications/push/


  // https://docs.joinmastodon.org/methods/search/

  async search(q, options = {}) {
    return this.get('api/v2/search', {
      q,
      ...options,
    });
  }


  // https://docs.joinmastodon.org/methods/instance/
  
  async getInstance() {
    return this.get('api/v1/instance', {}, { public: true });
  }

  async getInstancePeers() {
   return this.get('api/v1/instance/peers', {}, { public: true }); 
  }

  async getInstanceActivity() {
    return this.get('api/v1/instance/activity', {}, { public: true });
  }


  // https://docs.joinmastodon.org/methods/instance/trends/
  
  async getTrendingTags(limit = undefined) {
    return this.get('api/v1/trends', { limit }, { public: true });
  }


  // https://docs.joinmastodon.org/methods/instance/directory/
  
  /**
   *  Fetch the user directory
   *
   *  @param  {Object} options
   *  @param  {string} options.offset How many accounts to skip before returning results
   *  @param  {string} options.limit  Max number of results to return
   *  @param  {string} options.order  'active' (default) or 'new'
   *  @param  {boolean} options.local Only return local accounts
   *  @return {APIResponse}         Response data will be an array of Accounts
   */
  async getDirectory(options = {}) {
    return this.get('api/v1/directory', {
      offset: options.offset || undefined,
      limit: options.limit || undefined,
      order: options.order || undefined,
      local: typeof options.local === 'boolean' ? options.local : undefined,
    }, { public: true });
  }


  // https://docs.joinmastodon.org/methods/instance/custom_emojis/
  
  async getEmojis() {
    return this.get('api/v1/custom_emojis', {}, { public: true });
  }
}


Object.assign(exports, {
  MastoBotAPI,
});
