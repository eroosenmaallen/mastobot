/**
 *  MastoBot Main Module
 *
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       May 2022
 *
 *  * MastoBotHttp  provides basic http method helpers
 *  * MastoBotAPI   extends that with wrappers for the Mastodon API endpoints
 *  * MastoBot      extends that further with fancier methods to do more stuff more easily
 *  * APIError      Error returned from HTTP/API methods when things go wrong
 *
 */

'use strict';

const { APIError, tootLength } = require('./util');
const { MastoBotHttp } = require('./mastobotHttp');
const { MastoBotAPI } = require('./mastobotAPI');
const { MastoBot } = require('./mastobot');

Object.assign(exports, {
  APIError,
  MastoBotHttp,
  MastoBotAPI,
  MastoBot,
  tootLength,
});
